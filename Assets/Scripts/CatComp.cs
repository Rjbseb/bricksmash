﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatComp : MonoBehaviour {
	void Start () {
		string SubCat = PlayerPrefs.GetString("SubCat", "");
		string loadCat = PlayerPrefs.GetString("LoadCat", "MainCategory");
		Instantiate(Resources.Load("Levels/"+SubCat+loadCat));
	}
	
	void OnApplicationQuit()
    {
        PlayerPrefs.DeleteKey("SubCat");
		PlayerPrefs.DeleteKey("LevelCat");
		PlayerPrefs.DeleteKey("LoadCat");
    }
}
