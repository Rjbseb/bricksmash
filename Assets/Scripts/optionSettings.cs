﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class optionSettings : MonoBehaviour {
	private int useTilt;
	private int maxLives;
	private int autoPaddle;
	private int onoff;
	private int newonoff;
	
	public bool isUseTiltButton = false;
	public bool isMaxLivesButton = false;
	public bool isAutoPaddleButton = false;
	
	public GameObject updateText;
	
	
	void Start() {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		checkSettingsExist();
		
		if(isUseTiltButton) {
			getSetting("useTilt");
		} else if(isMaxLivesButton) {
			getSetting("maxLives");
		} else if(isAutoPaddleButton) {
			getSetting("autoPaddle");
		}
	}
	
	void OnMouseUp() {
		if(isUseTiltButton) {
			updateSetting("useTilt");
		} else if(isMaxLivesButton) {
			updateSetting("maxLives");
		} else if(isAutoPaddleButton) {
			updateSetting("autoPaddle");
		}

	
	}
	void checkSettingsExist() {
		if(PlayerPrefs.HasKey("useTilt")==false) {
			PlayerPrefs.SetInt("useTilt", 0);
		}
		if(PlayerPrefs.HasKey("maxLives")==false) {
			PlayerPrefs.SetInt("maxLives", 0);
		}
		if(PlayerPrefs.HasKey("autoPaddle")==false) {
			PlayerPrefs.SetInt("autoPaddle", 0);
		}
	}

	void getSetting(string setting) {
		int onoff=PlayerPrefs.GetInt(setting, 0);
		GetComponent<Renderer>().material.color = onoff == 1 ? Color.white : new Color(0.2F, 0.2F, 0.2F, 1F);
		if(updateText) {
			updateText.GetComponent<Renderer>().material.color = onoff == 0 ? Color.white : new Color(1F, 0.8F, 0.8F, 1F);
		}
		if(PlayerPrefs.GetInt("autoPaddle", 0) == 1  || PlayerPrefs.GetInt("maxLives", onoff) == 1){
			PlayerPrefs.SetInt("usingCheat", 1);
		} else {
			PlayerPrefs.SetInt("usingCheat", 0);
		}
	}
	
	void updateSetting(string setting) {
		int onoff=PlayerPrefs.GetInt(setting, 0);
		if(onoff == 1) {
			onoff = 0;
			PlayerPrefs.SetInt(setting, 0);
		} else {
			onoff = 1;
			PlayerPrefs.SetInt(setting, 1);
		}
		GetComponent<Renderer>().material.color = onoff == 1 ? Color.white : new Color(0.2F, 0.2F, 0.2F, 1F);
		if(updateText) {
			updateText.GetComponent<Renderer>().material.color = onoff == 0 ? Color.white : new Color(1F, 0.8F, 0.8F, 1F);
		}
		
		if(PlayerPrefs.GetInt("autoPaddle", 0) == 1  || PlayerPrefs.GetInt("maxLives", onoff) == 1){
			PlayerPrefs.SetInt("usingCheat", 1);
		} else {
			PlayerPrefs.SetInt("usingCheat", 0);
		}
	}
}
