﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPlayer : MonoBehaviour {
	public float maxLeft = -16f;
	public float maxRight = 16f;
	public float playerZ = -16.5f;
	public float playerOffset = 2;
	
	void Start()
	{
	}

    void FixedUpdate () 
    {
		transform.position = new Vector3 (Mathf.Clamp (GameObject.Find("MenuBall").transform.position.x+playerOffset, maxLeft, maxRight), 0f,playerZ);
    }
	
}
