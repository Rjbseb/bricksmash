﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadLevels : MonoBehaviour {
	public bool SubCategoryReady = false;
	
	void Start() {
		if (!SubCategoryReady) {
			Destroy(GameObject.Find(gameObject.name+"Arrow"));
		}
	}
	
	void OnMouseUp()
    {
		if (SubCategoryReady) {
			PlayerPrefs.SetString("LevelCat", gameObject.name);
			SceneManager.LoadScene("LevelList", LoadSceneMode.Single);
		}
    }
	
	void OnApplicationQuit()
    {
        PlayerPrefs.DeleteKey("SubCat");
		PlayerPrefs.DeleteKey("LevelCat");
		PlayerPrefs.DeleteKey("LoadCat");
    }
}
