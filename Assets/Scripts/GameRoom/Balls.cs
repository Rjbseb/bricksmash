﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balls : MonoBehaviour {
	public BallDetails Ball;
	public FieldSizes Field;
	public static Rigidbody rb;
	public static Balls instance = null;
	public static Controller Controller;
	public Camera MainCamera;

	void Awake () {
		rb = GetComponent<Rigidbody>();
	}
	
	void Start () {
		instance = this;
		Controller = GameObject.Find("Controller").GetComponent<Controller>();
		//MainCamera
	}
	
	void Update() {
		Vector3 newTorque = new Vector3(rb.velocity.z,rb.velocity.y,-rb.velocity.x);
		rb.AddTorque((Vector3.Normalize(newTorque) * Ball.Force)*Ball.Speed * Time.deltaTime, ForceMode.Force);
	}

	void FixedUpdate () {
		if (! Ball.InPlay && Input.GetButtonDown("Fire1")) {
			MainCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
			float newX = Map(Screen.width*MainCamera.rect.x,(Screen.width*MainCamera.rect.x+MainCamera.pixelWidth),Field.Left,Field.Right,Input.mousePosition.x);
			float newZ = Map(Screen.height*MainCamera.rect.y,(Screen.height*MainCamera.rect.y+MainCamera.pixelHeight),Field.Bottom,Field.Top,Input.mousePosition.y);
			if(newZ >= Ball.MinimumFireZ) {
				Ball.InPlay = true;
				Vector3 targetDir = new Vector3(newX,Ball.Y,newZ) - transform.position;
				Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, 180.0f, 1.0F);
				rb.AddForce((newDir * Ball.Force)*Ball.Speed, ForceMode.Force);
			}
		} else if (!Ball.InPlay) {
			transform.position = new Vector3 (GameObject.FindGameObjectsWithTag("Player")[0].transform.position.x+Ball.Offset, Ball.Y,Ball.Z);
		}
	}		
	
	void OnCollisionEnter (Collision col) {
		if(Ball.InPlay) {
			if(rb.velocity.z == 0) {
				rb.AddForce(new Vector3(0, 0, -100));
			}
			if(rb.velocity.x < 0.5 && rb.velocity.x > -0.5 && PlayerPrefs.GetInt("BypassVertCheck", 0) == 0) {
				if(transform.position.x<0 && rb.velocity.z >= 0) {
					rb.AddForce(new Vector3(100, 0, 0));
				} else if(transform.position.x<0 && rb.velocity.z < 0) {
					rb.AddForce(new Vector3(100, 0, 0));
				} else if (transform.position.x >=0 && rb.velocity.z >= 0) {
					rb.AddForce(new Vector3(-100, 0, 0));
				} else if (transform.position.x >=0 && rb.velocity.z < 0) {
					rb.AddForce(new Vector3(-100, 0, 0));
				}
			}
		}
		
		if(col.gameObject.tag == "DeathWall") {
			Controller.BallDeath(gameObject.name.Replace("(Clone)",""),gameObject);
		} else if(col.gameObject.tag == "Wall" || col.gameObject.tag == "Player") {
			Controller.ResetBonus();
		} else if(col.gameObject.tag.Contains("Block")) {
			Controller.UpdateScore(col.gameObject);
		}
	
	}
	
	public float Map(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue) {
		float OldRange = (OldMax - OldMin);
		float NewRange = (NewMax - NewMin);
		float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;
		NewValue = NewValue > NewMax ? NewMax : NewValue;
		NewValue = NewValue < NewMin ? NewMin : NewValue;
		return(NewValue);
	}
}

[System.Serializable]
public class FieldSizes {
	public float Left = -11;
	public float Right = 11;
	public float Top = 41;
	public float Bottom = -3;
}

[System.Serializable]
public class BallDetails {
	public bool InPlay = false;
	public float Speed = 1.5f;
	public float Force = 1500.0f;
	public float X = 0;
	public float Y = 0.75f;
	public float Z = -13;
	public float Offset = -2;
	public float MinimumFireZ = 5;
}