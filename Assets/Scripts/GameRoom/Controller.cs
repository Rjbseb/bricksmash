﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Controller : MonoBehaviour {

	public PlayerInfo Player;
	public TextObjects Text;
	
	void Awake () {
		//ClearBlocks();
		if(PlayerPrefs.GetInt("maxLives", 0) == 1) {
			Player.Lives=999;
		}
	}
	
	void Start () {
		if(PlayerPrefs.GetInt("usingCheat", 0) == 1) {
			GameObject.Find("ScoreStr").GetComponent<Renderer>().material.color = new Color(1F, 0.8F, 0.8F, 1F);
		}
		
		if(GameObject.FindGameObjectsWithTag("Level").Length > 0) {
			Text.Level.text = GameObject.FindGameObjectsWithTag("Level")[0].name.Replace("(Clone)","");
		} else {
			Text.Level.text = "Unknown Name";
		}
		Player.Blocks = CountBlocks();
		Text.Lives.text = Player.Lives.ToString("D3");
		Text.Score.text = Player.Score.ToString("D5");
		Text.Blocks.text = Player.Blocks.ToString();
	}

	public void UpdateScore(GameObject Block) {
		Player.Blocks--;
		Player.Score += int.Parse(Block.tag.Replace("Block:", "")) * Player.Bonus;
		Player.Bonus++;
		Block.tag = "Untagged";
		Block.SetActive(false);
		Destroy(Block);
		if(Player.Score >= Player.LastEarnedLife + Player.PointsForLife) {
			int count=Mathf.FloorToInt((Player.Score-Player.LastEarnedLife)/Player.PointsForLife);
			Player.LastEarnedLife += Player.PointsForLife*count;
			Player.Lives += count;
		}
		Text.Blocks.text = Player.Blocks.ToString();
		Text.Score.text = Player.Score.ToString();
		if(Player.Blocks == 0) {
				LoadScore();
		}
	}
	
	public void LoadScore() {
		Debug.Log(Player.Score);
	}
	
	public void LoadOOL() {
		Debug.Log("Out Of Lives");
	}
	
	public void ResetBonus() {
		if(!Player.BonusLocked) {
			Player.Bonus =1;
		}
	}
	
	public void BallDeath(string BallPrefab, GameObject OldBall) {
		Player.Lives--;
		Player.Bonus=1;
		Text.Lives.text = Player.Lives.ToString("D3");
		Instantiate(Resources.Load("Prefabs/Player/"+BallPrefab));
		Destroy(OldBall);
	}
	
	public int CountBlocks() {
		int SingBlocks = GameObject.FindGameObjectsWithTag("Block:10").Length;
		int DoubBlocks = GameObject.FindGameObjectsWithTag("Block:20").Length;
		int TripBlocks = GameObject.FindGameObjectsWithTag("Block:30").Length;
		int QuadBlocks = GameObject.FindGameObjectsWithTag("Block:40").Length;
		int QuinBlocks = GameObject.FindGameObjectsWithTag("Block:50").Length;
		int SexaBlocks = GameObject.FindGameObjectsWithTag("Block:60").Length;
		return (SingBlocks + DoubBlocks + TripBlocks + QuadBlocks + QuinBlocks + SexaBlocks);
	}
	
	public void ClearBlocks() {
		GameObject[] ClearBlocks = GameObject.FindGameObjectsWithTag ("Block:10");
		for(var i = 0 ; i < ClearBlocks.Length ; i ++) {
			Destroy(ClearBlocks[i]);
		}
		ClearBlocks = GameObject.FindGameObjectsWithTag ("Block:20");
		for(var i = 0 ; i < ClearBlocks.Length ; i ++) {
			Destroy(ClearBlocks[i]);
		}
		ClearBlocks = GameObject.FindGameObjectsWithTag ("Block:30");
		for(var i = 0 ; i < ClearBlocks.Length ; i ++) {
			Destroy(ClearBlocks[i]);
		}
		ClearBlocks = GameObject.FindGameObjectsWithTag ("Block:40");
		for(var i = 0 ; i < ClearBlocks.Length ; i ++) {
			Destroy(ClearBlocks[i]);
		}
		ClearBlocks = GameObject.FindGameObjectsWithTag ("Block:50");
		for(var i = 0 ; i < ClearBlocks.Length ; i ++) {
			Destroy(ClearBlocks[i]);
		}
		ClearBlocks = GameObject.FindGameObjectsWithTag ("Block:60");
		for(var i = 0 ; i < ClearBlocks.Length ; i ++) {
			Destroy(ClearBlocks[i]);
		}
	}
	
}

[System.Serializable]
public class PlayerInfo {
	public int PointsForLife = 1000;
	public int Lives = 3;
	public int Score = 0;
	public int Bonus = 1;
	public int Blocks = 0;
	public bool BonusLocked = false;
	public int LastEarnedLife = 0;
}

[System.Serializable]
public class TextObjects {
	public TextMesh Lives;
	public TextMesh Score;
	public TextMesh Blocks;
	public TextMesh Level;
}