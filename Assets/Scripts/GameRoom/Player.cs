﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public float paddleSpeed = 1.5f;
	public static float DoubleTapTime = 0.5f;  //Half a second before reset
	
	public float maxLeft = -19.0f;
	public float maxRight = 19.0f;
	public float playerZ = -16.5f;
	public float autoPaddleXOffset = 2.0f;	
	#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
		private float ButtonCooler = DoubleTapTime;
		private int ButtonCount = 0; // Times Button Presses
		private int ButtonNeeded = 2;
		private Vector3 theAcceleration;
		private Vector3 fixedAcceleration;
		private Quaternion calibrationQuaternion;
	#endif
 
	void CalibrateAccelerometer()
	{
		#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
			Vector3 accelerationSnapshot = Input.acceleration;
			Quaternion rotateQuaternion = Quaternion.FromToRotation(
			new Vector3(0.0f, 0.0f, -1.0f), accelerationSnapshot);
			calibrationQuaternion = Quaternion.Inverse(rotateQuaternion);
		#endif
	}
	
	void Start()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
			if (PlayerPrefs.GetInt("useTilt", 0) == 1) {
				CalibrateAccelerometer();
			}
		#endif
	}

    void FixedUpdate () 
    {
		if(PlayerPrefs.GetInt("autoPaddle", 0) == 1) {
			transform.position = new Vector3 (GameObject.FindGameObjectsWithTag("Ball")[0].transform.position.x+autoPaddleXOffset, 0,playerZ);
			//transform.position += new Vector3 (-paddleSpeed, 0.0f, 0f);
			transform.position  = new Vector3 (Mathf.Clamp (transform.position.x, maxLeft, maxRight), 0f, playerZ);
		} else {
		#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR || UNITY_STANDALONE_LINUX
            transform.position += new Vector3 (Input.GetAxis ("Horizontal") * paddleSpeed, 0.0f, 0f);
			transform.position  = new Vector3 (Mathf.Clamp (transform.position.x, maxLeft, maxRight), 0f, playerZ);
			
		#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
			if (PlayerPrefs.GetInt("useTilt", 0) == 1) {
				if ( Input.GetKeyDown(KeyCode.Escape) ) {
					if ( ButtonCooler > 0 && ButtonCount == ButtonNeeded-1) {
						CalibrateAccelerometer();
					} else {
						ButtonCooler = DoubleTapTime; 
						ButtonCount += 1 ;
					}
				}
 
				if ( ButtonCooler > 0 ) {
					ButtonCooler -= 1 * Time.deltaTime ;
				} else {
					ButtonCount = 0 ;
				}
			
				Vector3 theAcceleration = Input.acceleration;
				Vector3 fixedAcceleration = calibrationQuaternion * theAcceleration;
				Vector3 movement = new Vector3 (fixedAcceleration.x, 0.0f, 0f);
				transform.position += movement * paddleSpeed;
				transform.position  = new Vector3 (Mathf.Clamp (transform.position.x, maxLeft, maxRight), 0f, playerZ);
			} else {
				if (Input.GetButton("Fire1")) {
					if(Input.mousePosition.y<=200){
						if(Input.mousePosition.x <= Screen.width/2) {
							transform.position += new Vector3 (-paddleSpeed, 0.0f, 0f);
							transform.position  = new Vector3 (Mathf.Clamp (transform.position.x, maxLeft, maxRight), 0f, playerZ);
						} else {
							transform.position += new Vector3 (paddleSpeed, 0.0f, 0f);
							transform.position  = new Vector3 (Mathf.Clamp (transform.position.x, maxLeft, maxRight), 0f, playerZ);
						}
					}
				}
			}
		#endif
		}
	}
}
