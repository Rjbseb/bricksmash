﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ButtonClick : MonoBehaviour {
	public bool isActive = false;
	public string loadScean;
	
	void start() {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}
	void OnMouseUp()
    {
		if(isActive) {
			if(loadScean == "Exit" || loadScean == "Quit" ) {
				Application.Quit();
			} else if(loadScean == "Categories") {
				PlayerPrefs.SetString("SubCat", "");
				PlayerPrefs.SetString("LoadCat", "MainCategory");
				SceneManager.LoadScene("Categories", LoadSceneMode.Single);
			} else {
				SceneManager.LoadScene(loadScean, LoadSceneMode.Single);
			}
		}
    }
}
