﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBall : MonoBehaviour {
	
	public static Rigidbody rb;
	public bool ballInPlay;
	public static MenuBall instance = null;	
	
	public float velocityRatio = 1.5f;
	public float defaultForce = 1400.0f;
	public float fieldLeft = -19f;
	public float fieldRight = 19f;
	public float fieldTop = 57f;
	public float fieldBottom = -21f;
	public float ballY = 0.75f;
	public float ballZ = -13.0f;
	public float ballOffset = -2.0f;
	
	void Start () {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		transform.position = new Vector3(transform.position.x,ballY, transform.position.z);
		if (instance == null) {
			instance = this;
		}
	}
	
	void Awake () {
		transform.position = new Vector3(transform.position.x,ballY, transform.position.z);
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if (! ballInPlay) {
			ballInPlay = true;
			transform.parent = null;
			float newX = Map(0,Screen.width,fieldLeft,fieldRight,Random.Range(0f,Screen.width));
			float newZ = Map(0,Screen.height,fieldBottom,fieldTop,Random.Range(200f,Screen.height));
			Vector3 targetDir = new Vector3(newX,ballY,newZ) - transform.position;
			Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, 90f, 0.0F);
			transform.rotation = Quaternion.LookRotation(newDir);
			rb.AddForce((transform.forward * defaultForce)*velocityRatio, ForceMode.Force);
			rb.AddTorque((transform.forward * defaultForce)*velocityRatio);
		}
	}
	
	void FixedUpdate () {
		if(ballInPlay) {
			float xvel = rb.velocity.x;
			float zvel = rb.velocity.z;
			if(xvel >= -3 && xvel < 0) {
				rb.AddForce(new Vector3(Random.Range(-100f,-400f)*velocityRatio,0,0));
			} else if(xvel <= 3 && xvel >= 0) {
				rb.AddForce(new Vector3(Random.Range(100f,400f)*velocityRatio,0,0));
			}
			
			if(zvel >= -3 && zvel < 0) {
				rb.AddForce(new Vector3(0,0,Random.Range(-100f,-400f)*velocityRatio));
			} else if(zvel <= 3 && zvel >= 0) {
				rb.AddForce(new Vector3(0,0,Random.Range(100f,400f)*velocityRatio));
			}
		}
	}
	
	void OnCollisionEnter (Collision col) {
		rb.angularVelocity = Vector3.zero;
		rb.AddTorque(new Vector3 (rb.velocity.z, 0, -rb.velocity.x), ForceMode.Impulse);
		transform.position = new Vector3(transform.position.x, ballY, transform.position.z);
		
		if(col.gameObject.tag == "Block") {
			//Destroy(col.gameObject);
			//col.gameObject.GetComponent<Renderer>().enabled = false;
			//col.gameObject.GetComponent<Renderer>()
			//rend.enabled = false;
		}
	}

	
	public float Map(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue) {
		float OldRange = (OldMax - OldMin);
		float NewRange = (NewMax - NewMin);
		float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;
        return(NewValue);
    }

}
