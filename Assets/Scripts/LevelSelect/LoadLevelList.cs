﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevelList : MonoBehaviour {
	private Object GamesListPrefab;
	private bool Ready = false;
	
	void Start() {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		string LoadRes = "Levels/"+transform.parent.name.Replace("(Clone)", "")+"/"+gameObject.name;
		GamesListPrefab = Resources.Load(LoadRes);
		if(!GamesListPrefab) {
			Ready = false;
			Destroy(GetComponent<Transform>().GetChild(2).gameObject);
		} else {
			Ready = true;
		}		
	}
	
	void OnMouseUp() {
		if(Ready) {
			Destroy(GameObject.FindWithTag("Menu"));
			Destroy(GameObject.FindWithTag("Ball"));
			Destroy(GameObject.FindWithTag("MenuBall"));
			Destroy(GameObject.FindWithTag("Player"));
			Instantiate(GamesListPrefab);
			PlayerPrefs.SetString("SubCat",gameObject.name.Replace("(Clone)", ""));
		}
	}

}
