﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadScore : MonoBehaviour {

	private string levelName;
	// Use this for initialization
	void Start () {
		levelName = transform.parent.GetComponent<Renderer>().sharedMaterial.name;
		//GetComponent<TextMesh>().text = levelName;
		GetComponent<TextMesh>().text = PlayerPrefs.GetInt(levelName, 0).ToString("D5").Replace("0","O");
		if(levelName == "ComingSoon") {
			GetComponent<TextMesh>().text = "SECRET";
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
