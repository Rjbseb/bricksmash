﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class LoadLevel : MonoBehaviour {
	private TextMesh ScoreText;
	private string ScorePref;
	private Object LayoutPrefab;
	private Object LevelPrefab;
	private string MainCat;
	private string SubCat;
	public enum LevelSizes {Size8Layout,Size12Layout,Size16Layout,Size20Layout,Size24Layout,Size28Layout,Size32Layout}
    public LevelSizes LevelSize;
	private GameObject[] MenuPrefab;

	
	void Start() {
		ScorePref=transform.parent.name+"_"+gameObject.name;
		PlayerPrefs.SetString(ScorePref, "00000");
		ScoreText=GetComponent<Transform>().GetChild(0).gameObject.GetComponent<TextMesh>();
		ScoreText.text=PlayerPrefs.GetString(ScorePref, "00000");
		MainCat=PlayerPrefs.GetString("MainCat", "Games");
		SubCat=PlayerPrefs.GetString("SubCat", "Games");
	}
	
	void OnMouseUp()
    {
		string LoadRes = "Levels/"+MainCat+"/"+SubCat+"/"+gameObject.name+"/"+gameObject.name;
		string LoadLayout = "Layouts/"+LevelSize;
		LevelPrefab = Resources.Load(LoadRes);
		LayoutPrefab = Resources.Load(LoadLayout);
		
		if(LevelPrefab && LayoutPrefab) {
				GameObject[] MenuPrefab = GameObject.FindGameObjectsWithTag ("Menu");
				for(var i = 0 ; i < MenuPrefab.Length ; i ++) {
					Destroy(MenuPrefab[i]);
				}
				
				GameObject[] Layouts = GameObject.FindGameObjectsWithTag ("Layout");
				for(var i = 0 ; i < Layouts.Length ; i ++) {
					Destroy(Layouts[i]);
				}
			
			Instantiate(LayoutPrefab);
			Instantiate(LevelPrefab,new Vector3(0, 0, 24), Quaternion.identity);
		}
    }
}