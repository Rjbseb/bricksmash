﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSubCat : MonoBehaviour {
	private Object SubCategoryPrefab;
	private bool Ready = false;
	public string CatBypass = "";
	
	void Start() {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		if(CatBypass != "") {
			string LoadRes = "Categories/"+CatBypass;
			SubCategoryPrefab = Resources.Load(LoadRes);
			TextMesh ScoreText=gameObject.GetComponent<TextMesh>();
			ScoreText.text="Back to "+CatBypass;
		} else {
			string LoadRes = "Categories/"+gameObject.name.Replace("Menu","");
			SubCategoryPrefab = Resources.Load(LoadRes);
			if(!SubCategoryPrefab) {
				Destroy(GetComponent<Transform>().GetChild(2).gameObject);
			}
		}
		
		if(!SubCategoryPrefab) {
				Ready = false;
		} else {
				Ready = true;
		}
	}
	
	void OnMouseUp() {
		if(Ready) {
			Destroy(GameObject.FindWithTag("Menu"));
			Destroy(GameObject.FindWithTag("Ball"));
			Destroy(GameObject.FindWithTag("MenuBall"));
			Destroy(GameObject.FindWithTag("Player"));
			Instantiate(SubCategoryPrefab);
			if(CatBypass != "") {
				PlayerPrefs.SetString("MainCat",CatBypass);
			} else {
				PlayerPrefs.SetString("MainCat",gameObject.name.Replace("Menu",""));
			}
		}
	}
}
