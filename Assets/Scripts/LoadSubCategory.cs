﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadSubCategory : MonoBehaviour {

	public bool SubCategoryReady = false;
	void Start() {
		if (!SubCategoryReady) {
			Destroy(GameObject.Find(gameObject.name+"Arrow"));
		}
	}
	
	void OnMouseUp()
    {
		if (SubCategoryReady) {
			string cat = PlayerPrefs.GetString("CurrentCategory", "Games");
			SceneManager.LoadScene(cat+gameObject.name, LoadSceneMode.Single);
		}
    }
}
