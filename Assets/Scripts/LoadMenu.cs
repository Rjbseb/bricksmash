﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadMenu : MonoBehaviour {
	public enum MenuList {None,MainMenu,Categories,Options,Store,Quit}
    public MenuList MenuToLoad;
	
	private string menu;
	
	void Start() {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		menu = MenuToLoad.ToString();
		if (!GameObject.Find("MenuLayout"))
		{
			foreach (GameObject o in Object.FindObjectsOfType<GameObject>()) { Destroy(o); }
			Instantiate(Resources.Load("Layouts/MenuLayout"));
		}
			GetComponent<TextMesh>().color = menu == "None" ? new Vector4(0.7f,0.7f,0.7f,1) : new Vector4(1,1,1,1);
	}
	
	void OnMouseUp()
    {
		if(menu != "None") {
			Destroy(GameObject.FindWithTag("Menu"));
			Destroy(GameObject.FindWithTag("Ball"));
			Destroy(GameObject.FindWithTag("MenuBall"));
			Destroy(GameObject.FindWithTag("Player"));
			if(menu =="Quit") {
				#if UNITY_EDITOR
					UnityEditor.EditorApplication.isPlaying = false;
				#elif UNITY_WEBPLAYER
					//Application.OpenURL(webplayerQuitURL);
				#else
					Application.Quit();
				#endif
			} else {
				Instantiate(Resources.Load("Menus/"+menu));
			}
		} else {
			Debug.Log("Error: Nothing to do Here");
		}
    }
}