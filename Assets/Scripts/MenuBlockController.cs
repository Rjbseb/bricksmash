﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MenuBlockController : MonoBehaviour {
	
	public float hideTimer = 5f;
	public bool visible = true;
	private float unhideTime;
	
	void OnCollisionEnter (Collision col) {
		if(col.gameObject.tag == "MenuBall") {
			GetComponent<Renderer>().enabled = false;
			GetComponent<Collider>().enabled = false;
			unhideTime = Time.time + hideTimer * Random.Range(1f,3f);
			visible = false;
		}
	}
	
	void FixedUpdate() {
		if(visible == false && Time.time >= unhideTime) {
			GetComponent<Renderer>().enabled = true;
			GetComponent<Collider>().enabled = true;
		}
		
	}
	
}
