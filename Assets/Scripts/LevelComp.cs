﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelComp : MonoBehaviour {
	void Start()
    {
		string SubCat = PlayerPrefs.GetString("SubCat", "");
		string LoadLevel = PlayerPrefs.GetString("LevelCat", "");
		Debug.Log("Levels/"+SubCat+LoadLevel+"/"+LoadLevel+"Levels");
		if(LoadLevel != "") {
			Instantiate(Resources.Load("Levels/"+SubCat+LoadLevel+"/"+LoadLevel+"Levels"));
		}
    }
	
}
